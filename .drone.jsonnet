local Pipeline(rubyVer, license, db, redmine) = {
  kind: "pipeline",
  name: rubyVer + "-" + db + "-" + redmine + "-" + license,
  steps: [
    {
      name: "tests",
      image: "redmineup/redmineup_ci",
      commands: [
        "service postgresql start && service mysql start && sleep 5",
        "export PATH=~/.rbenv/shims:$PATH",
        "export CODEPATH=`pwd`",
        "/root/run_for.sh redmine_cms ruby-" + rubyVer + " " + db + " redmine-" + redmine
      ]
    }
  ]
};

[
  Pipeline("3.1.0", "pro", "mysql", "trunk"),
  Pipeline("3.1.0", "pro", "pg", "trunk"),
  Pipeline("3.1.0", "pro", "mysql", "5.0"),
  Pipeline("2.7.3", "pro", "mysql", "4.2"),
  Pipeline("2.4.1", "pro", "mysql", "4.0"),
  Pipeline("2.4.1", "pro", "mysql", "3.4"),
  Pipeline("2.2.6", "pro", "mysql", "3.4"),
  Pipeline("2.2.6", "pro", "pg", "3.0")
]
